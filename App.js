import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import StartScreen from './app/views/StartScreen';
import Game from './app/views/game/Game';

export default function App() {
  const [isStartGame, setStartGame] = useState(false);
  const [isGameOver, setGameOver] = useState(false);
  const [score, setScore] = useState(0);
  const [isWon, setIsWon] = useState(false);
  const isStartScreen = !isStartGame || isGameOver || isWon;
  return (
    <View style={styles.container}>
      {isStartScreen && (
        <StartScreen
          setStartGame={setStartGame}
          isGameOver={isGameOver}
          setGameOver={setGameOver}
          score={score}
          setScore={setScore}
          setIsWon={setIsWon}
          isWon={isWon}
        />
      )}
      {!isStartScreen && (
        <Game
          setGameOver={setGameOver}
          score={score}
          setScore={setScore}
          setIsWon={setIsWon}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff'
  }
});

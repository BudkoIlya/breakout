import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  Button,
  Share
} from 'react-native';
import { SCREEN_HEIGHT } from './utils';

const StartScreen = ({
  setStartGame,
  isGameOver,
  setGameOver,
  score,
  setScore,
  isWon,
  setIsWon
}) => {
  const startGame = () => {
    setStartGame(true);
    setGameOver(false);
    setIsWon(false);
    setScore(0);
  };

  const onShare = async () => {
    await Share.share({
      message: `My breakout score : ${score}`
    });
  };
  return (
    <TouchableOpacity style={styles.startGame} onPress={startGame}>
      {isGameOver || isWon ? (
        <View style={{ flexDirection: 'column' }}>
          <Text style={{ textAlign: 'center' }}>
            {isGameOver ? 'Game over. Press to start again' : 'You won'}
          </Text>
          <Text style={{ textAlign: 'center' }}>Your score: {score}</Text>
          <View style={styles.shareButton}>
            <Button onPress={onShare} title='Share' />
          </View>
        </View>
      ) : (
        <Text>Press to start</Text>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  shareButton: {
    marginTop: SCREEN_HEIGHT / 10
  },
  startGame: {
    flex: 1,
    width: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default StartScreen;

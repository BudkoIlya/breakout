import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

export const scale = size => (width / guidelineBaseWidth) * size;
export const verticalScale = size => (height / guidelineBaseHeight) * size;

export const SCREEN_WIDTH = width;
export const SCREEN_HEIGHT = height;

export const BOTTOM_BLOCK_POSITION = SCREEN_HEIGHT / 10;
export const HEIGHT_BOTTOM_BLOCK = 30;
export const WIDTH_BOTTOM_BLOCK = (SCREEN_WIDTH / 10) * 4;

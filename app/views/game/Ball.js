import React, { useRef, useEffect } from 'react';
import { StyleSheet } from 'react-native';
import {
  HEIGHT_BOTTOM_BLOCK,
  WIDTH_BOTTOM_BLOCK,
  SCREEN_HEIGHT,
  SCREEN_WIDTH,
  BOTTOM_BLOCK_POSITION,
  verticalScale
} from '../utils';
import { View } from 'react-native';

const WIDTH_BALL = verticalScale(25);

const initialCoordinates = {
  y: SCREEN_HEIGHT / 10 + HEIGHT_BOTTOM_BLOCK,
  x: SCREEN_WIDTH / 2 - WIDTH_BALL / 2
};
const SPEED = { x: 40, y: -160 };

const Ball = ({
  blocksCoordinates,
  positionBottomBlock,
  rerender,
  setGameOver,
  score,
  setScore,
  setIsWon
}) => {
  const ball = useRef();
  const position = useRef({ ...initialCoordinates });

  const prevTime = useRef({ prevTime: null });
  const moveXY = useRef({ ...SPEED });
  const remainBlocks = useRef();

  const checkGameOver = y => {
    if (y - WIDTH_BALL < BOTTOM_BLOCK_POSITION) {
      return true;
    }
  };

  const checkWallCollision = (x, y) => {
    if (
      y + WIDTH_BALL >= SCREEN_HEIGHT ||
      x + WIDTH_BALL >= SCREEN_WIDTH ||
      x <= 0
    ) {
      return true;
    }
  };

  const checkBottomBlockCollision = (x, y) => {
    if (
      y >= positionBottomBlock.current.y &&
      y <= positionBottomBlock.current.y + HEIGHT_BOTTOM_BLOCK &&
      x + WIDTH_BALL >= positionBottomBlock.current.x &&
      x <= positionBottomBlock.current.x + WIDTH_BOTTOM_BLOCK
    ) {
      return true;
    }
  };
  const checkLeftRightCollision = (block, x) => {
    return moveXY.current.x > 0
      ? block.x - (x + WIDTH_BALL)
      : block.x + block.width - x;
  };

  const checkTopBottomCollision = (block, y) => {
    return moveXY.current.y > 0
      ? block.y + block.height - (SCREEN_HEIGHT - y - WIDTH_BALL)
      : block.y - (SCREEN_HEIGHT - y);
  };

  const checkCollision = (x, y) => {
    let foundBlock;
    for (let block of remainBlocks.current) {
      if (
        SCREEN_HEIGHT - y >= block.y && // top
        SCREEN_HEIGHT - y - WIDTH_BALL <= block.y + block.height && // bot
        x + WIDTH_BALL >= block.x && // left
        x <= block.x + block.width // right
      ) {
        foundBlock = {
          ...block,
          collisionY: Math.abs(checkTopBottomCollision(block, y)),
          collisionX: Math.abs(checkLeftRightCollision(block, x))
        };
        break;
      }
    }
    if (foundBlock) {
      blocksCoordinates.current = blocksCoordinates.current.map(el => {
        if (el.index === foundBlock.index) {
          return { ...el, isDeleted: true };
        }
        return el;
      });
      rerender({});
      remainBlocks.current = remainBlocks.current.filter(
        ({ index }) => index !== foundBlock.index
      );
    }
    return foundBlock;
  };

  const animation = frame => {
    const speed = (frame - prevTime.current.prevTime) / 1000;

    position.current.y = position.current.y + moveXY.current.y * speed;
    position.current.x = position.current.x + moveXY.current.x * speed;
    ball.current.setNativeProps({
      style: {
        bottom: position.current.y,
        left: position.current.x
      }
    });
    prevTime.current.prevTime = frame;

    const collision = checkCollision(position.current.x, position.current.y);
    if (collision) {
      setScore((score += 1));
      if (collision.collisionX - collision.collisionY > 0) {
        moveXY.current.y = -moveXY.current.y;
      } else {
        moveXY.current.x = -moveXY.current.x;
      }
    }

    const bottomCollision = checkBottomBlockCollision(
      position.current.x,
      position.current.y
    );
    if (bottomCollision) {
      moveXY.current.y = -moveXY.current.y;
    }

    const wallCollision = checkWallCollision(
      position.current.x,
      position.current.y
    );

    if (wallCollision) {
      if (
        position.current.x + WIDTH_BALL >= SCREEN_WIDTH ||
        position.current.x <= 0
      ) {
        moveXY.current.x = -moveXY.current.x;
      } else if (position.current.y + WIDTH_BALL >= SCREEN_HEIGHT) {
        moveXY.current.y = -moveXY.current.y;
      }
    }

    const animationId = requestAnimationFrame(animation);

    const isGameOver = checkGameOver(position.current.y);
    if (score === 20 || isGameOver) {
      position.current = { ...initialCoordinates };
      moveXY.current = { ...SPEED };
      cancelAnimationFrame(animationId);
      isGameOver ? setGameOver(true) : setIsWon(true);
    }
  };

  useEffect(() => {
    setTimeout(() => {
      if (ball.current && blocksCoordinates.current.length === 20) {
        remainBlocks.current = blocksCoordinates.current;
        requestAnimationFrame(time => {
          prevTime.current.prevTime = time;
          animation(time);
        });
      }
    }, 500);
  }, [ball]);

  return <View style={styles.ball} ref={ball} />;
};

const styles = StyleSheet.create({
  ball: {
    position: 'absolute',
    bottom: initialCoordinates.y,
    left: initialCoordinates.x,
    width: WIDTH_BALL,
    height: WIDTH_BALL,
    backgroundColor: `black`,
    borderRadius: WIDTH_BALL
  }
});

export default Ball;

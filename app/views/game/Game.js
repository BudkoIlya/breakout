import React, { useState, useRef } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import Blocks from './Blocks';
import BottomBlock from './BottomBlock';
import Ball from './Ball';
import {
  BOTTOM_BLOCK_POSITION,
  SCREEN_WIDTH,
  verticalScale,
  scale
} from '../utils';

const Game = ({ setGameOver, setScore, score, setIsWon }) => {
  const blocksCoordinates = useRef([]);

  const [foundIndexesBlocks, setIndexesBlocks] = useState([]);
  const [, rerender] = useState();
  const bottomBlock = useRef();

  const positionBottomBlock = useRef({
    x: (SCREEN_WIDTH / 10) * 3,
    y: BOTTOM_BLOCK_POSITION
  }); // center of window for block

  const findBlock = id => {
    setIndexesBlocks([...foundIndexesBlocks, id]);
  };
  return (
    <View style={styles.game}>
      <Blocks
        blocksCoordinates={blocksCoordinates}
        foundIndexesBlocks={foundIndexesBlocks}
      />
      <Ball
        setGameOver={setGameOver}
        blocksCoordinates={blocksCoordinates}
        positionBottomBlock={positionBottomBlock}
        findBlock={findBlock}
        rerender={rerender}
        score={score}
        setScore={setScore}
        setIsWon={setIsWon}
      />
      <BottomBlock
        bottomBlock={bottomBlock}
        position={positionBottomBlock}
        rerender={rerender}
      />
      <Text style={styles.score}>Score - {score}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  score: {
    position: 'absolute',
    top: verticalScale(50),
    left: scale(20),
    fontWeight: 'bold',
    fontSize: verticalScale(20),
    color: 'rgb(24, 24, 24)'
  },
  game: {
    display: 'flex',
    width: '100%',
    height: '100%',
    backgroundColor: '#fff'
  }
});

export default Game;

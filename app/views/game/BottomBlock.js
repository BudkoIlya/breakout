import React, { useRef } from 'react';
import { StyleSheet, PanResponder } from 'react-native';
import { SCREEN_WIDTH } from '../utils/index';
import {
  BOTTOM_BLOCK_POSITION,
  HEIGHT_BOTTOM_BLOCK,
  WIDTH_BOTTOM_BLOCK
} from '../utils/index';
import { View } from 'react-native';

const BottomBlock = ({ position, bottomBlock }) => {
  const prevTime = useRef({ prevTime: null });

  const animation = (frame, direction) => {
    if (
      position.current.x + WIDTH_BOTTOM_BLOCK < SCREEN_WIDTH &&
      position.current.x > 0
    ) {
      position.current.x = position.current.x + 1.5 * direction;
    } else if (position.current.x + WIDTH_BOTTOM_BLOCK >= SCREEN_WIDTH) {
      position.current.x = position.current.x - 0.1;
    } else if (position.current.x <= 0) {
      position.current.x = position.current.x + 0.1;
    }
    bottomBlock.current.setNativeProps({
      style: { left: position.current.x }
    });
    prevTime.current.prevTime = frame;
  };
  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderGrant: () => {},
      onPanResponderMove: (event, state) => {
        if (state.dx) {
          const direction = state.dx > 0 ? 1 : -1;
          requestAnimationFrame(time => {
            prevTime.current.prevTime = time;
            animation(time, direction);
          });
        }
      },
      onPanResponderRelease: () => {}
    })
  ).current;
  return (
    <View
      style={styles.bottomBlock}
      {...panResponder.panHandlers}
      ref={bottomBlock}
    />
  );
};

const styles = StyleSheet.create({
  bottomBlock: {
    left: (SCREEN_WIDTH / 10) * 3,
    position: 'absolute',
    bottom: BOTTOM_BLOCK_POSITION,
    width: WIDTH_BOTTOM_BLOCK,
    height: HEIGHT_BOTTOM_BLOCK,
    backgroundColor: 'black'
  }
});

export default BottomBlock;

import React from 'react';
import { View, StyleSheet } from 'react-native';
import { SCREEN_HEIGHT } from '../utils';

const Blocks = ({ blocksCoordinates }) => {
  const blocks = Array(20).fill(1);

  const setCoordinatesBlocks = (event, i) => {
    const layout = event.nativeEvent.layout;
    blocksCoordinates.current = [
      ...blocksCoordinates.current,
      {
        ...layout,
        index: i,
        y: layout.y + SCREEN_HEIGHT / 5,
        del: false
      }
    ].sort((a, b) => a.index - b.index);
  };

  return (
    <View style={styles.blocks}>
      {blocks.map((v, i) => (
        <View
          key={i}
          style={[
            styles.block,
            {
              backgroundColor:
                blocksCoordinates.current &&
                blocksCoordinates.current[i] &&
                blocksCoordinates.current[i].isDeleted
                  ? 'white'
                  : 'rgb(72, 72, 72)'
            }
          ]}
          onLayout={event => setCoordinatesBlocks(event, i)}
        />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  blocks: {
    position: 'absolute',
    top: SCREEN_HEIGHT / 5,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  block: {
    width: '20%',
    height: SCREEN_HEIGHT / 20,
    borderWidth: 1,
    borderColor: 'white'
  }
});

export default Blocks;
